/** @type {import('tailwindcss').Config} */
module.exports = {
	content: ['./src/**/*.{html,js,svelte,ts}'],
	theme: {
		extend: {
			container: {
				center: true,
				padding: {
					DEFAULT: '1rem',
					md: '2rem',
					lg: '3rem',
					xl: '4rem'
				}
			},
			// classes are with absolute values until the 100-900 scale is done.
			backgroundImage: {
				purpleBright: 'radial-gradient(50% 50% at 50% 50%, #8F3AB7 0%, transparent 100%);',
				bright:
					'radial-gradient(50% 50% at 50% 50%, var(--tw-gradient-from), var(--tw-gradient-to));'
			},
			boxShadow: {
				pupleBright: '21px 6px 38px -10px hsl(var(--pf));',
				button: '0px 4px 4px hsl(var(--b2)), inset 0px 2px 4px hsl(var(--pc));',
				pressed:
					'0px 4px 4px rgba(20, 31, 69, 0.5), inset 4px 5px 10px #141F45, inset 0px 2px 4px rgba(244, 251, 255, 0.5);'
			},
			animation: {
				marquee: 'marquee 5s linear infinite',
				stars: 'stars 20s linear infinite',
				glow: 'glow 1s linear infinite alternate',
				vanishUp: 'vanishUp 2s',
				vanishLeft: 'vanishLeft 2s',
				slideX: 'slideX 2s',
				slideY: 'slideY 2s'
			},
			keyframes: {
				marquee: {
					'0%': { transform: 'translateX(0%)' },
					'100%': { transform: 'translateX(-100%)' }
				},
				stars: {
					'0%': { transform: 'translateY(0%)' },
					'100%': { transform: 'translateY(-100%)' }
				},
				glow: {
					'0%': { opacity: '0.8', transform: 'scale(1)' },
					'100%': { opacity: '0.3', transform: 'scale(.7)' }
				},
				vanishUp: {
					'0%': { transform: 'translateY(0%)', opacity: 1 },
					'85%': { transform: 'translateY(0%)', opacity: 1 },
					'100%': { transform: 'translateY(-50%)', opacity: 0 }
				},
				vanishLeft: {
					'0%': { transform: 'translateX(0%)', opacity: 1 },
					'85%': { transform: 'translateX(0%)', opacity: 1 },
					'100%': { transform: 'translateX(-50%)', opacity: 0 }
				},
				slideY: {
					'0%': { transform: 'translateY(50%)', opacity: 0 },
					'15%': { transform: 'translateY(0%)', opacity: 1 },
					'85%': { transform: 'translateY(0%)', opacity: 1 },
					'100%': { transform: 'translateY(-50%)', opacity: 0 }
				},
				slideX: {
					'0%': { transform: 'translateX(30%)', opacity: 0 },
					'15%': { transform: 'translateX(0%)', opacity: 1 },
					'85%': { transform: 'translateX(0%)', opacity: 1 },
					'100%': { transform: 'translateX(-30%)', opacity: 0 }
				}
			},
			fontFamily: {
				montserrat: ['montserrat', 'Verdana ', 'serif'],
				workSans: ['Work Sans', 'Helvetica', 'sans-serif'],
				dmMono: ['DM Mono', 'monospace']
			},
			colors: {
				darkPurple: {
					900: '#8F3AB7'
				}
			}
		}
	},
	plugins: [require('daisyui')],
	daisyui: {
		themes: [
			{
				kelp_theme: {
					primary: '#9747FF',
					'primary-focus': '#673AB7',
					'primary-content': '#F4FBFF',
					secondary: '#791EA4',
					'secondary-focus': '#9938BA',
					'secondary-content': '#6A53C5',
					accent: '#BB64DC',
					neutral: '#FFFFFF',
					'base-100': '#070D20',
					'base-200': '#141F45',
					'base-300': '#1F2655',
					'base-content': '#F4FBFF',
					info: '#D1C4E9',
					success: '#36D399',
					warning: '#FBBD23',
					error: '#F87272'
				}
			}
		]
	}
};
