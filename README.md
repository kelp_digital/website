# Kelp Digital website

<p align="center" width="100%">
    <img src="https://ipfs.io/ipfs/QmRHnPw8t8zPhA19SYszScSV7p4NBLhziqtT2zcQJvNoip?filename=kelp_twitter_card_home.png"
    alt='kelp seaweed on starry dark purple background with white Kelp Digital logo'> 
</p>

This is the repo for [Kelp Digital website](https://kelp.digital)

## Get started

Install the dependencies...

```bash
pnpm install
```

To launch the website run the following command:

```bash
pnpm dev
```

Navigate to [localhost:5173](http://localhost:5173/). You should see the app running. Edit a component file in `src`, save it to see your changes.

## Preview

To build the preview run the following code:

```bash
pnpm build
pnpm preview
```

Navigate to [localhost:4173](http://localhost:4173/).

## Deploying to the ipfs

Go to the CI section and manually trigger the `upload-to-ipfs`.
Then copy the given link and share it with the team!
