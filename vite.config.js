import { sveltekit } from '@sveltejs/kit/vite';
import { resolve } from 'node:path';

/** @type {import('vite').UserConfig} */
const config = {
	plugins: [sveltekit()],
	resolve: {
		alias: {
			$src: resolve('./src')
		}
	}
};

export default config;
